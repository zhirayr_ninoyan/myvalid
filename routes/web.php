<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/author/post', 'PostController@getPostForm')->name('post.form');
Route::post('/author/post', 'PostController@createPost')->name('post.form');
Route::get('/author/post/detail/{post}', 'PostController@getPost')->name('post.detail');
Route::get('/author/post/edit/{post}', 'PostController@editPost')->name('post.edit');
Route::post('/author/post/edit/{post}', 'PostController@updatePost')->name('post.update');
Route::get('/author/post/delete/{post}', 'PostController@deletePost')->name('post.delete');

Route::get('/usersPage/{user}', 'UserController@goToUsersPage')->name('users.page');

Route::post('/author/post/detail/{post}/comment', 'CommentController@addComment')->name('comment.add');

Route::post('/author/post/detail/{post}/likedpost', 'LikeController@likePost')->name('post.like');

Route::post('/author/post/detail/{id}/likedcomment', 'LikeController@likeComment')->name('comment.like');

Route::post('/usersPage/{user}/friendrequest', 'FriendshipController@addFriend')->name('add.friend');
Route::get('/home/acceptfriendrequest/{user}', 'FriendshipController@acceptFriendRequest')->name('accept.friend.request');
Route::get('/home/deletefriendrequest/{user}', 'FriendshipController@deleteFriendRequest')->name('delete.friend.request');

Route::get('/usersPage/{chat}/chat', 'ChatController@goToChatPage')->name('chat.page');
Route::post('/usersPage/{chat}/chat/addtochat', 'ChatController@addToChat')->name('add.to.chat');
Route::post('/usersPage/{chat}/chat/message', 'MessageController@sendMessage')->name('message.send');



