@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                <div>
                    <a href="{{route('home')}}">Back to Posts</a>
                </div>
            </nav>

            <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <h1>{{ $post->title }}</h1>
                <div class="col-sm-8 blog-main">
                    <p>{{ $post->description }}</p>
                    @if($loggedInUser->id == $post->user_id)
                    <a href="{{ route('post.edit', ['post' => $post->id]) }}">
                        <button type="button" class="btn btn-primary btn-sm">Edit Post</button>
                    </a>
                    <a href="{{ route('post.delete', ['post' => $post->id]) }}">
                        <button type="button" class="btn btn-danger btn-sm">Delete Post</button>
                    </a>
                    @endif
                </div>
                <form method="post" action="{{ route('post.like', ['post' => $post->id]) }}">
                {{csrf_field()}}
                    <div class="postLikeBox">
                        <button type="sumbit">Like</button>
                        <span>{{$numberOfPostLikes}}</span>
                    </div>
                </form>
                <form method="post" action="{{ route('comment.add', ['post' => $post->id]) }}">
                {{ csrf_field() }}
                <div class="form-group postCommentBox">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>
                </div>
                <div>
                    <button type="submit">Add Comment</button>
                </div>
                </form>
                @foreach ($post->comments as $comment) 
                <div class="col-sm-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong>{{$user}}</strong>
                        </div>
                        <div class="panel-body">
                        {{$comment->comment}}
                        </div><!-- /panel-body -->
                        <form method="post" action="{{ route('comment.like', ['id' => $post->id]) }}">
                            {{csrf_field()}}
                            <div class="postLikeBox">
                                <button type="sumbit">Like</button>
                                <span>{{$numberOfCommentLikes}}</span>
                            </div>
                        </form>
                    </div><!-- /panel panel-default -->
                </div><!-- /col-sm-5 -->
                @endforeach
            </main>

           
        </div>
    </div>
@endsection