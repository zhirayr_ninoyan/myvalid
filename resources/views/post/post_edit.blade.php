@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
                <div>
                    <a href="{{route('home')}}">Back to Posts</a>
                </div>
            </nav>

            <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
                <h1>Edit Post</h1>
                <div class="col-md-4">
                    <form method="post" action="{{ route('post.update',  ['id' => $post->id]) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Title</label>
                            <input type="text" class="form-control" id="id_title" name="title"
                                   aria-describedby="title" placeholder="Enter title" value="{{$post->title}}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="id_description" rows="3" name="description" placeholder="Description" >{{$post->description}}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit post</button>
                    </form>
                </div>
            </main>
        </div>
    </div>
@endsection