@extends('layouts.app')

@section('content')

<div style="width:100%" class="float-left"> 
    <div class="float-left">   
        <form  method="post" action="{{route('message.send' , ['chat' => $chat])}}">
        {{ csrf_field() }}
            <input type="text" name="message" id="">

            <button type="submit">Send Message</button>   
        </form>
    </div>
    <div class="float-left">
        <form  method="post" action="{{route('add.to.chat' , ['chat' => $chat])}}">
        {{ csrf_field() }}
            <select name="username" id="">
                    @foreach ($users as $userToAdd)
                    <option value="{{$userToAdd->id}}">{{$userToAdd->name}}</option>
                    @endforeach     
            </select>
            <button type="submit">Add To Chat</button>   
        </form>
    </div>
</div>
<div class="float-left">
    @foreach($chat->messages as $message)
        <div>
            {{$chat->users->find($message->user_id)->name}}
        </div>
        <div>
            {{$message->message}}
        </div>

    @endforeach
</div>

@endsection