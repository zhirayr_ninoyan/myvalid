@extends('layouts.app')


@section('content')
<div>
    <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
        <div>
            <a href="{{route('home')}}">Back to Posts</a>
        </div>
        <div>
            <form action="{{route('add.friend',['user' => $user->id])}}" method="post">
                {{ csrf_field() }}
                @if(Auth::user()->id != $user->id )
                <button>Add Friend</button>
                @endif
            </form>
        </div>
        <div>
            <a class="button" href="{{route('chat.page', ['chat' => $chat->id])}}">Start Chat</a>
        </div>
    </nav>
</div>
<div>
    
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Author</th>
            <th>Learn more</th>
            <th>Created on</th>
        </tr>
        </thead>
        <tbody>
        @if($posts)
            @foreach($posts as $post)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $post->title }}</td>
                    <td>{{ $user->name }}</td>
                    <td>
                    <a href="{{ route('post.detail', ['id' => $post->id]) }}">view more</a>
                    </td>
                    <td>{{ Carbon\Carbon::parse($post->created_at)->format('d-m-Y')  }}</td>
                </tr>
            @endforeach
        @else
            <p class="text-center text-primary">No Posts created Yet!</p>
        @endif
    </table>
</div>
@endsection

