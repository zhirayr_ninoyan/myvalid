@extends('layouts.app')

@section('content')

<div class="float-left allUsers">
    @if($friendRequest)
        <p>You have friend request</p>
        <form action="{{route('accept.friend.request', ['user' => $friendRequest->user_id])}}">
            <button>Accept</button>
        </form>
        <form action="{{route('accept.friend.request', ['user' => $friendRequest->user_id])}}">
            <button>Delete</button>
        </form>
    @endif    
</div>
<div class="float-left allUsers">
    <ul>
        @foreach($users as $user)
        <li><a href="{{route('users.page', ['user' => $user->id])}}">{{$user->name}} </a></li>
        @endforeach
    </ul>
</div>
<div class="container float-left">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ Auth::user()->name }} You are logged in!
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
            <div class= "col-md-8 ">
                <h1>Posts
                    <a href="{{ route('post.form') }}">
                        <button type="button" class="btn btn-primary btn-sm">Create Post</button>
                    </a>
                </h1>
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Learn more</th>
                        <th>Created on</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($posts)
                        @foreach($posts as $post)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td>{{ $post->title }}</td>
                                <td>{{ $userId->name }}</td>
                                <td>
                                <a href="{{ route('post.detail', ['post' => $post->id]) }}">view more</a>
                                </td>
                                <td>{{ Carbon\Carbon::parse($post->created_at)->format('d-m-Y')  }}</td>
                            </tr>
                        @endforeach
                    @else
                        <p class="text-center text-primary">No Posts created Yet!</p>
                    @endif
                </table><Br>
            </div>
    </div>
    <div class="frindslist">
        @foreach($friendslist as $friends)
            <div>
                
            </div>
        @endforeach
    </div>
</div>


@endsection

   

