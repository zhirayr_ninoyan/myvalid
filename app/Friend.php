<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $fillable = ['user_id', 'friends_id'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_friend');
    }
}
