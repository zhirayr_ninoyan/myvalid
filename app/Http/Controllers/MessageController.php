<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\User;
use App\Chat;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function sendMessage(Chat $chat) {

        $user = Auth::user();

        $message = $user->messages()->create(array(

            'message' => Input::get('message'),
            'chat_id' => $chat->id

        ));

        return redirect()->route('chat.page', ['chat' => $chat])->with('success', 'Message succesfully sent');

    }

}
