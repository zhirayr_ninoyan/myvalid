<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Friend;
use App\Chat;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;


class UserController extends Controller
{
    public function goToUsersPage(User $user) {
       
        $chat = Auth::user()->chats()->whereHas('users' , function (Builder $query) use($user) {
            
        return $query->where('users.id', $user->id);
    
        })->first();

        if(is_null($chat)) {
     
            Chat::create()->users()->attach([$user->id, Auth::user()->id]);
        }

        
       
        $posts = $user->posts;
        
        return view('users/usersPage', ['posts' => $posts, 'user' => $user, 'chat' => $chat]);
    }
}
