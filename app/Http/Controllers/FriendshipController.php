<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Friend;
use App\FriendRequest;
use App\Chat;
use Illuminate\Support\Facades\Auth;
use App\User;



class FriendshipController extends Controller
{
    public function addFriend(User $user ) {

        $loggedInUser = Auth::user();

        if($loggedInUser->id != $user->id &&  is_null(FriendRequest::where('user_id' , $user->id)->first()) && is_null(FriendRequest::where('requested_user_id' , $user->id)->first()) ) {
            $friendRequest = FriendRequest::create(array(

                'user_id' => $loggedInUser->id,
                'requested_user_id' => $user->id
    
            ));
        }

        return redirect()->route('users.page', [ 'user' => $user ])->with('success', 'Friend requested succesfully sent');
    }


    public function acceptFriendRequest (User $user) {

        $friendRequest = FriendRequest::where('requested_user_id', Auth::user()->id )->first();

        Friend::create()->users()->attach([$user->id, Auth::user()->id]);

        FriendRequest::where('requested_user_id', Auth::user()->id )->first()->delete();

        return redirect()->route('home', [ 'user' => $user, 'friendRequest' => $friendRequest ]);
    }

    public function deleteFriendRequest (User $user) {

        $friendRequest = FriendRequest::where('requested_user_id', Auth::user()->id )->first();

        $deleteFriendRequest = FriendRequest::where('requested_user_id', Auth::user()->id )->first()->delete();

        return redirect()->route('home', [ 'user' => $user, 'friendRequest' => $friendRequest ]);
    }
}
