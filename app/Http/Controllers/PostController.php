<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Comment;
use App\Like;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPostForm() {
        return view('post/post_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createPost(Request $request){
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
        ]);
        
        
        $post = Post::create(array(
            'title' => Input::get('title'),
            'description' => Input::get('description'),
            'user_id' => Auth::user()->id
        ));

        
    
        return redirect()->route('home')->with('success', 'Post has been successfully added!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPost(Post $post){
        $user = User::find($post->user_id)->name;

        $loggedInUser = Auth::user();
        $postLikes = $post->likes();
        $commentLikes = Comment::find($post->id);

        
        if( !is_null($postLikes)) {

            $numberOfPostLikes = $post->likes()->count();

        }
        else {

            $numberOfPostLikes = 0;
        }

        if( !is_null($commentLikes)) {

            $numberOfCommentLikes = Comment::find($post->id)->likes()->count();

        }
        else {

            $numberOfCommentLikes = 0;
        }

        return view('post/post_detail', ['post' => $post, 'loggedInUser' => $loggedInUser, 'user' => $user, 'numberOfPostLikes' => $numberOfPostLikes,'numberOfCommentLikes' => $numberOfCommentLikes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function editPost (Post $post) {
    
        return view('post/post_edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePost(Request $request , Post $post) {
        $this->authorize('update', $post);

        $post->title = $request->title;
        $post->description = $request->description;
        $post->save();
        return redirect()->route('home')->with('success', 'Post has been updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletePost(Post $post){
        $post->delete();
        return redirect()->route('home')->with('success', 'Post has been deleted successfully!');
    }
}
