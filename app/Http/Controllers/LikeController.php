<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Like;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class LikeController extends Controller
{
    public function likePost(Request $request , Post $post)
    {
        $user = Auth::user();

        $usersLike = $post->likes()->where('user_id', $user->id);

        if($usersLike->count() == 0) {

            $like = $post->likes()->create(array(
                'user_id' => Auth::user()->id,
            ));
        }
        else {
            $usersLike->delete();          
        }
        return redirect()->route('post.detail', ['post' => $post->id])->with('success', 'Post has been successfully liked!');
    }

    public function likeComment(Request $request , Post $id)
    {

        $user = Auth::user();

        $usersLike = $user->likes()->where('user_id', $user->id);

        if($usersLike->count() == 0) {
            $like = Comment::find($id->id)->likes()->create(array(
                'user_id' => Auth::user()->id,
            ));
        }
        else {
            $usersLike->delete();           
        }
        return redirect()->route('post.detail', ['id' => $id->id])->with('success', 'Comment has been successfully liked!');
    }
}
