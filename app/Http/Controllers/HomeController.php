<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\FriendRequest;
use App\Friend;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user();
        $posts = User::find($userId->id)->posts;
        $users = DB::table('users')->get();
        $friendRequest = FriendRequest::where('requested_user_id', Auth::user()->id )->first();
        $friendslist = User::find($userId->id)->friends;
        return view('home', ['posts' => $posts, 'users' => $users, 'userId' => $userId, 'friendRequest' => $friendRequest, 'friendslist' => $friendslist ]);
        
    }
    
}
