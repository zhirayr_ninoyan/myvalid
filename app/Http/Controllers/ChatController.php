<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function goToChatPage(Chat $chat) {

        $authUser = Auth::user();

        $chatUsers = $chat->users->pluck('id')->toArray();

        $users = User::whereNotIn('id' , $chatUsers)->get();

        return view('users/chat', ['chat' => $chat, 'authUser' => $authUser, 'users' => $users]);
    }


    public function addToChat (Request $request, Chat $chat) {

        $user = User::find($request->username);

        $idsOfChatUsers = $chat->users->pluck('id')->toArray();

        if(count($chat->users) == 2) {

            $groupChat = Chat::create();
            
            $groupChatUsers = $groupChat->users()->attach([$idsOfChatUsers[0], $idsOfChatUsers[1],  $user->id]);

        }
        else {

            $chat->users()->attach($user->id);

            $groupChat = $chat;
        }

        return redirect()->route('chat.page', ['chat' => $groupChat->id]);
    }
}
