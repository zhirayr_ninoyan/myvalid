<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $fillable = [
        'user_id', 'chat_id', 'message'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function chats()
    {
        return $this->belongsTo(Chat::class);
    }
}
